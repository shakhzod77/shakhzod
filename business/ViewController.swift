//
//  ViewController.swift
//  business
//
//  Created by ios developer) on 5/29/19.
//  Copyright © 2019 ios developer). All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    var returnedData = [jsonstruct]()
    var email : NSArray = []
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.estimatedRowHeight = 100;
        self.tableView.rowHeight = UITableView.automaticDimension
        loadData()
    }
    
    func loadData()
    {
        //        Alamofire.request("https://yestravell.com/api/business").responseJSON { response in
        //            guard response.result.isSuccess else {
        //                print("Ошибка при запросе данных \(String(describing: response.result.error))")
        //                return
        //            }
        //            if(response.data != nil){
        //            do {
        //                if let data = response.data {
        //                    let objects = try! JSON(data: data).dictionaryObject as! [Dictionary<String,Any>]
        //                    objects.forEach { (data) in
        //                        do {
        //                            let object = try JSONDecoder().decode(DataModel.self, from: response.data!)
        //                        } catch {
        //
        //                        }
        //                    }
        //                }
        //            } catch {
        //
        //            }
        //            print(response.value)
        //        }
        //        }
        //        Alamofire.request("https://yestravell.com/api/business").responseJSON { response in
        //            guard response.result.isSuccess else {
        //                print("Ошибка при запросе данных \(String(describing: response.result.error))")
        //                return
        //            }
        //            print(response.value)
        //        }
        guard let url = URL(string: "https://yestravell.com/api/business") else {return}
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let dataResponse = data,
                error == nil else {
                    print(error?.localizedDescription ?? "Response Error")
                    return }
            do{
                //here dataResponse received from a network request
                let jsonResponse = try JSONSerialization.jsonObject(with:
                    dataResponse, options: [])
                // print(jsonResponse) //Response result
                
                guard let jsonArray = jsonResponse as? [[String: Any]] else {
                    return
                }
                // print(jsonArray)
                guard let title = jsonArray[0]["title"] as? String else { return }
                // print(title) // delectus aut autem
                
                for dic in jsonArray{
                    guard let title = dic["address"] as? String else { return }
                    print(title) //Output
                }
                
                // var model = [jsonstruct]() //Initialising Model Array
                for dic in jsonArray{
                    self.returnedData.append(jsonstruct(dic)) // adding now value in Model array
                }
                
                //  print(model[0].additional_email) // 1211
                DispatchQueue.main.async {
                    self.tableView.reloadData();
                }
            } catch let parsingError {
                print("Error", parsingError)
            }
        }
        task.resume()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return returnedData.count
    }
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            self.tableView.register(UINib(nibName: "CustomTableViewCell", bundle: nil), forCellReuseIdentifier: "CustomTableViewCell")
            let cell = tableView.dequeueReusableCell(withIdentifier: "CustomTableViewCell") as! CustomTableViewCell
            cell.imgView.image = UIImage(named: "1")
            cell.emailLabel.text = self.returnedData[indexPath.row].title
            cell.phoneLabel.text = self.returnedData[indexPath.row].phone
            cell.addressLabel.text = self.returnedData[indexPath.row].address
            cell.selectionStyle = .none
            return cell
    }
        
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

