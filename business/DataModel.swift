//
//  DataModel.swift
//  business
//
//  Created by ios developer) on 5/30/19.
//  Copyright © 2019 ios developer). All rights reserved.
//

import Foundation

struct jsonstruct:Codable {
    let additional_email:String
    let address:String
    let agent_id:String
    let city:String
    let city_slug:String
    let created_at:String
    let description:String
    let featured:String
    let id:String
    let image:String
    let location_latitude:String
    let location_longitude:String
    let logotype:String
    let phone:String
    let phone_local:String
    let slug:String
    let social_fb:String
    let social_inst:String
    let social_twitter:String
    let social_youtube:String
    let title:String
    let updated_at:String
    let video:String
    let view_count:String
    let website:String
    
    init(_ dictionary: [String: Any]) {
        
        self.additional_email = dictionary["additional_email"] as? String ?? "no email"
        self.address = dictionary["address"] as? String ?? "no address"
        self.agent_id = dictionary["agent_id"] as? String ?? ""
        self.city = dictionary["city"] as? String ?? ""
        self.city_slug = dictionary["city_slug"] as? String ?? ""
        self.created_at = dictionary["created_at"] as? String ?? ""
        self.description = dictionary["description"] as? String ?? ""
        self.featured = dictionary["featured"] as? String ?? ""
        self.id = dictionary["id"] as? String ?? ""
        self.image = dictionary["image"] as? String ?? ""
        self.location_latitude = dictionary["location_latitude"] as? String ?? ""
        self.location_longitude = dictionary["location_longitude"] as? String ?? ""
        self.logotype = dictionary["logotype"] as? String ?? ""
        self.phone = dictionary["phone"] as? String ?? "no phone"
        self.phone_local = dictionary["phone_local"] as? String ?? ""
        self.slug = dictionary["slug"] as? String ?? ""
        self.social_fb = dictionary["social_fb"] as? String ?? ""
        self.social_inst = dictionary["social_inst"] as? String ?? ""
        self.social_twitter = dictionary["social_twitter"] as? String ?? ""
        self.social_youtube = dictionary["social_youtube"] as? String ?? ""
        self.title = dictionary["title"] as? String ?? ""
        self.updated_at = dictionary["updated_at"] as? String ?? ""
        self.video = dictionary["video"] as? String ?? ""
        self.view_count = dictionary["view_count"] as? String ?? ""
        self.website = dictionary["website"] as? String ?? ""
    }
    
}
